CREATE DATABASE IF NOT EXISTS topBDxml1;
USE topBDxml1;
DROP TABLE IF EXISTS aluno;
DROP TABLE IF EXISTS curso;
CREATE TABLE curso(
    codigo INT NOT NULL,
    descricao VARCHAR(60),
    cargaHoraria INT,
    PRIMARY KEY(codigo)
);
CREATE TABLE aluno(
    matricula INT NOT NULL,
    nome VARCHAR(60),
    email VARCHAR(60),
    cod_curso INT,
    PRIMARY KEY(matricula),
    FOREIGN KEY (cod_curso) REFERENCES curso(codigo)
);

INSERT INTO curso VALUES( 12, "Node.js", 90);
INSERT INTO curso VALUES( 9, "Ruby on Rails", 90);
INSERT INTO curso VALUES( 3, "Django", 90);
INSERT INTO curso VALUES( 4, "C++ e POO", 120);
INSERT INTO curso VALUES( 8, "JavaScript", 60);
INSERT INTO curso VALUES( 16, "Inglês", 90);
INSERT INTO curso VALUES( 21, "HTML5 e CSS4", 60);
INSERT INTO curso VALUES( 23, "Oracle Database", 60);
INSERT INTO curso VALUES( 11, "Android", 120);
INSERT INTO curso VALUES( 13, "Arduino", 45);

INSERT INTO aluno VALUES( 2345, "Kássio Venicius", "klepro@temp.com", 16);
INSERT INTO aluno VALUES( 2451, "Pedro Vitor", "pbossal@temp.com", 9);
INSERT INTO aluno VALUES( 3245, "Walace Tales", "wobexiga@temp.com", 21);
INSERT INTO aluno VALUES( 5999, "Edson Machado", "edaxe@temp.com", 8);
INSERT INTO aluno VALUES( 9366, "Pablo Vinicius", "parrocha@temp.com", 12);
INSERT INTO aluno VALUES( 8345, "Kaio Rego", "krego@temp.com", 3);
INSERT INTO aluno VALUES( 7325, "Marcos Joshoa", "mrobot@temp.com", 4);
INSERT INTO aluno VALUES( 3587, "Marcelo Ramos", "kairom@temp.com", 11);
INSERT INTO aluno VALUES( 2741, "Artur Miranda", "arturmiranda@temp.com", 13);
INSERT INTO aluno VALUES( 7392, "Pedro Pinto", "pedrotrue@temp.com", 3);
INSERT INTO aluno VALUES( 6377, "Francisco Mendes", "cjoel@temp.com", 23);
INSERT INTO aluno VALUES( 3748, "Jeova Gomes", "jeotaro@temp.com", 21);
INSERT INTO aluno VALUES( 8832, "Rafael Martins", "rafaboy@temp.com", 4);
INSERT INTO aluno VALUES( 4535, "José Reis", "zereis@temp.com", 12);


