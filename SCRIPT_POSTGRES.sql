DROP TABLE IF EXISTS aluno;
DROP TABLE IF EXISTS curso;
CREATE TABLE Curso(
    codigo INT NOT NULL,
    descricao VARCHAR(60),
    cargaHoraria INT,
    PRIMARY KEY(codigo)
);
CREATE TABLE Aluno(
    matricula INT NOT NULL,
    nome VARCHAR(60),
    email VARCHAR(60),
    cod_curso INT,
    PRIMARY KEY(matricula),
    FOREIGN KEY (cod_curso) REFERENCES curso(codigo)
);