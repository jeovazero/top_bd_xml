//##       POSTGRES      ##

const pg = require('pg');
const connString = process.env.DATABASE_URL || 'postgres://baobab:elefante@localhost/baobab';

function insert(data, n, strLoc, table, func, callback) {
    console.log("FIRST " + table);
    pg.connect(connString, function (err, client, done) {
        console.log("init " + table)
        if (err) {
            return console.error("Erou!", err);
        }
        var arr = new Array(n);
        console.log(`inserindo ${table}`);
        var prom = []
        data.forEach(function (elem, i) {
            if (strLoc[i % n] == 1) {
                arr[i % n] = `\'${elem}\'`;
            } else arr[i % n] = elem + ' ';

            if (i % n == n - 1) {
                //                console.log(arr.join(','))
                prom.push(
                    client.query(
                        `INSERT INTO ${table} VALUES(${arr.join(',')});`,
                        function (err, result) {
                            if (err) {
                                return console.error('error running query', err);
                            }
                            console.log("RESULTADO: " + table + " " + result);
                        }
                    )
                );
                console.log("query");
            }
        });
        Promise.all(prom).then(function () {
            client.end();
            if (func == true) {
                callback();
            }
        });
        console.log(`Termino ${table} 1`);
    });
    console.log(`Terminou ${table} 2`);
}
/*
const rd = require('readline');
const rl = rd.createInterface({
    input: process.stdin,
    output: process.stdout
});
*/
const fs = require('fs');

var xml = "";
var rd_n = 0;
var aluno_data = null;
var curso_data = null;
fs.readFile('cursos.xml', 'utf8', function (err, data) {
    if (err) throw err;
    xml = data;
    var resultado = XP("//row/codigo/text() | //row/descricao/text() | //row/cargaHoraria/text()");
    //console.log(resultado[0] + " " + resultado[1] + " " + resultado[2]);
    curso_data = resultado;
    rd_n++;
    if (rd_n == 2) {
        console.log("ACABOU AQUI CURSO");
        inserts();
    }
});
fs.readFile('alunos.xml', 'utf8', function (err, data) {
    if (err) throw err;
    xml = data;
    var resultado = XP("//row/matricula/text() | //row/nome/text() | //row/email/text() | //row/cod_curso/text()");
    aluno_data = resultado;
    rd_n++;
    if (rd_n == 2) {
        console.log("ACABOU AQUI aluno");
        inserts();
    }
});

function inserts() {
    var f = function () {
        insert(aluno_data, 4, [0, 1, 1, 0], 'aluno');
    }
    insert(curso_data, 3, [0, 1, 0], 'curso', true, f);
    console.log("pass");
}

var xpath = require('xpath'),
    dom = require('xmldom').DOMParser;

function XP(comand) {
    var doc = new dom().parseFromString(xml);
    var title = xpath.select(comand, doc);
    return title;
}
/*
function read(data) {
    console.log("reading data...  ");
    var comand = " ";
    rl.question('Digita o comando: ', (answer) => {
        comand = answer;
        console.log("xpath: " + comand);
        XP(comand);
    });
}
*/
