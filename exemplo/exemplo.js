const rd = require('readline');
const rl = rd.createInterface({
    input: process.stdin,
    output: process.stdout
});

var fs = require('fs');
var xml = "";
fs.readFile('books.xml', 'utf8', function(err, data){
    if(err) throw err;
    xml = data;
    read(data);
})
var xpath = require('xpath'),
    dom = require('xmldom').DOMParser;
function XP(comand){
    var doc = new dom().parseFromString(xml);
    var title = xpath.select(comand, doc);
    console.log("TITLE: >>" + title);
}
function read(data){
    console.log("reading data...  ");
    var comand = " ";
    rl.question('Digita o comando: ', (answer) => {
        comand = answer;
        console.log("xpath: " + comand);
        XP(comand);
    });
}
