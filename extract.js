//##       MYSQL      ##
const mysql = require('mysql');
var conn = mysql.createConnection({
    host: "localhost",
    user: "baobab",
    password: "fulero",
    database: "topBDxml1"
})
var result = null;
conn.connect(function (err) {
    if (err) throw err;
    conn.query('select * from curso;', function (err, resultado) {
        if (err) throw err;
        var inxml = resultado;
        console.log('\n\n' + inxml[0] + '\n\n');
        toXML(inxml, 'cursos');
    });
    conn.query('select * from aluno;', function (err, resultado) {
        if (err) throw err;
        var inxml = resultado;
        console.log('\n\n' + inxml[0] + '\n\n');
        toXML(inxml, 'alunos');
    });
});

const fs = require('fs');

var xmltext = '<?xml version="1.0" encoding="UTF-8"?>';

function toXML(inputxml, set) {
    var tag, content, text, rows;
    content = '';
    console.log("JSON: " + JSON.stringify(inputxml));
    inputxml.forEach(function (obj) {
        rows = '';

        for (var attr in obj) {
            rows += `\n\t\t<${attr}>${obj[attr]}</${attr}>`;
        }
        tag = 'row';
        content += `\n\n\t<${tag}>${rows}\n\t</${tag}>`;
    });
    tag = set;
    content = xmltext + `\n<${tag}>${content}\n</${tag}>`;
    console.log(content);
    fs.writeFile(set + '.xml', content, function (err) {
        if (err) throw err;
        console.log("OK sauvou");
    });
}
/*
const rd = require('readline');
const rl = rd.createInterface({
    input: process.stdin,
    output: process.stdout
});

var fs = require('fs');
var xml = "";
fs.readFile('books.xml', 'utf8', function (err, data) {
    if (err) throw err;
    xml = data;
    read(data);
})
var xpath = require('xpath'),
    dom = require('xmldom').DOMParser;

function XP(comand) {
    var doc = new dom().parseFromString(xml);
    var title = xpath.select(comand, doc);
    console.log("TITLE: >>" + title);
}

function read(data) {
    console.log("reading data...  ");
    var comand = " ";
    rl.question('Digita o comando: ', (answer) => {
        comand = answer;
        console.log("xpath: " + comand);
        XP(comand);
    });
}
*/
