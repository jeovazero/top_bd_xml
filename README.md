## TRABALHO TOPICOS EM BANCO DE DADOS
### _Migração de dados entre SGBD's com XML_
> O trabalho consiste em extrair dados de tabelas de um banco para um _XML_ e depois inserir os dados desse _XML_ em outro banco.

### **Requesitos básicos:**
* **Node.js** >= v7.10.0
* **NPM** >= 4.2.0

> [Installing Node.js via package manager](https://nodejs.org/en/download/package-manager/)

### Requesitos de módulos do Node.js:
* pg (postgresql)
* mysql
* xpath
* xmldom

> [Pesquise o módulo e siga as instruções](https://www.npmjs.com/)

### Preparando:
1. Entre no Mysql no mesmo diretório da aplicação e execute o '_scripBD.sql_'

    ```mysql
       mysql -u user -p
    ```

    ```
       source SCRIPT_MYSQL.sql
    ```

2. Para o Postgres no mesmo diretório da aplicação execute o 'psql.sql'

    ```shell
      psql -f SCRIPT_POSTGRES.sql
    ```

3. Edite o '_extract.js_' nas linhas 5 e 6, mude o nome de usuário e senha para os apropriados do seu Mysql.

4. Edite o '_migrate.js_' nas linha 4, mude o nome de usuário, senha e nome do database apropriados do seu Postgres:

    ```javascript
      'postgres://user:password@localhost/database_name'
    ```

### Executando

1. Para **Mysql >> XML**,  no diretório da aplicação execute:

    ```shell
      node extract.js
    ```

    Os arquivos ***alunos.xml*** e ***cursos.xml*** foram gerados.

2. Para **XML >> Postgres**, no diretório da aplicação execute:

    ```shell
       node migrate.js
    ```

   Se tudo ocorrer bem a migração dos dados terá sucesso.